import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Single Page Application - Angular';

  ngOnInit(): void {
    const button1 = document.getElementById('0');
    const button2 =  document.getElementById('1');
    const searching = document.getElementById('input');
    // @ts-ignore
    searching.addEventListener('keyup', this.search);
    // @ts-ignore
    button1.addEventListener('click', this.changePage);
    // @ts-ignore
    button2.addEventListener('click', this.changePage);
  }

  //@ts-ignore
  changePage(e) {
    e.preventDefault();

    switch (e.target.id) {
      case '0':
        console.log(e.target.id);
        break;
      case '1':
        console.log(e.target.id);
        break;
      default:
        break;
    }
  }

  search(e: any){
    let input = document.getElementById('input');
    // @ts-ignore
    let upper = input.value.toUpperCase();
    let ul = document.getElementsByTagName('article');

    for (let i = 0; i < ul.length; i++){
      let p = ul[i].getElementsByTagName('p')[0];
      let txt = p.textContent || p.innerText;

      if (txt.toUpperCase().indexOf(upper) > -1) {
        ul[i].style.display = '';
      } else {
        ul[i].style.display = 'none';
      }
    }
  }
}
