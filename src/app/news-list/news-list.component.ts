import { Component, OnInit } from '@angular/core';
import { news } from '../../assets/mock-news';
import { deleteAll } from '../../assets/deleteAll';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const select = document.getElementById('dropdown');
    // @ts-ignore
    select.addEventListener('click', this.dropdown);

    deleteAll();
    this.placeArticles();
  }

  placeArticles(){
    const element = document.createElement('div');
    const body = document.getElementsByTagName('body')[0];

    element.setAttribute('id', 'div1');
    element.setAttribute('class', 'container');
    body.appendChild(element);

    news.forEach((news, index) => {
      const para = document.createElement('article');

      para.setAttribute('id', `newsList${index}`);
      // @ts-ignore
      element.appendChild(para);

      const img = document.createElement('img');
      const article = document.getElementById(`newsList${index}`);

      img.src = news.img;
      // @ts-ignore
      article.appendChild(img);

      const containerDiv = document.createElement('div');
      const el = document.getElementById(`newsList${index}`);

      containerDiv.setAttribute('class', 'ul-container');
      containerDiv.id = `ul-container${index}`;
      // @ts-ignore
      el.appendChild(containerDiv);

      const cross = document.createElement('button');
      const text = document.createTextNode('✖');
      const elem = document.getElementById(`ul-container${index}`);

      cross.appendChild(text);
      cross.addEventListener('click',  () => {
        if(confirm('Are you sure you want to remove this article?')){
          // @ts-ignore
          document.getElementById(`newsList${index}`).remove();
          // @ts-ignore
          document.getElementById(`newsList${index}`).removeEventListener('click', this);
        }
      });
      // @ts-ignore
      elem.appendChild(cross);

      const para1 = document.createElement('h3');
      const node1 = document.createTextNode(news.title);
      const element1 = document.getElementById(`ul-container${index}`);

      para1.appendChild(node1);
      // @ts-ignore
      element1.appendChild(para1);

      const para2 = document.createElement('p');
      const node2 = document.createTextNode(news.summary);
      const element2 = document.getElementById(`ul-container${index}`);

      para2.appendChild(node2);
      // @ts-ignore
      element2.appendChild(para2);

      const para3 = document.createElement('p');
      const node3 = document.createTextNode(news.date.toDateString());
      const element3 = document.getElementById(`ul-container${index}`);

      para3.appendChild(node3);
      // @ts-ignore
      element3.appendChild(para3);
    });
  }

  dropdown(){
    const select = document.getElementById('dropdown');
    //@ts-ignore
    switch (select.value){
      case 'da':
        // @ts-ignore
        news.sort((a, b) => b.date - a.date);
        break;
      case 'dd':
        // @ts-ignore
        news.sort((a, b) => a.date - b.date);
        break;
      case 't':
        news.sort((a, b) => a.title.localeCompare(b.title));
        break;
      default:
        news.sort((a, b) => a.title.localeCompare(b.title));
        break;
    }

    deleteAll();
    this.placeArticles();
  }
}
